<?php

/**
 * @file
 * Hook available for the api module.
 */

/**
 * Alter the definitions of parsing functions for files.
 *
 * You can implement this hook to parse new extensions or the parser used in
 * existing ones.
 *
 * This hook is invoked by \Drupal\api\Parser::parseFunctions().
 *
 * @param array $parse_functions
 *   List of parsing functions by extension.
 */
function hook_api_parse_functions_alter(array &$parse_functions) {
  // Parse '.foo' files as PHP.
  $parse_functions['foo'] = 'parsePhp';
}

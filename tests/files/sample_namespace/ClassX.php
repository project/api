<?php

/**
 * @file Contains \api\test2\ClassX.
 */

namespace api\test2;

/**
 * Another sample class in a namespace.
 */
class ClassX {
  /**
   * An exciting static method.
   */
  public static function xMethod() {
  }
}

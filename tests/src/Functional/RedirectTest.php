<?php

namespace Drupal\Tests\api\Functional;

/**
 * Tests the API module redirect functionality.
 *
 * @group api
 */
class RedirectTest extends WebPagesBase {

  /**
   * Tests redirect functionality.
   */
  public function testRedirectIfWrongBranch() {
    // File pages.
    $this->drupalGet('api/' . $this->branchInfo['project'] . '/sample.php/' . $this->branchInfo['branch_name'] . '.x');
    $this->assertTrue(str_ends_with($this->getUrl(), 'api/' . $this->branchInfo['project'] . '/sample.php/' . $this->branchInfo['branch_name']), 'File pages redirect failed.');

    // Object pages.
    $this->drupalGet('api/' . $this->branchInfo['project'] . '/sample.php/function/sample_function/' . $this->branchInfo['branch_name'] . '.x');
    $this->assertTrue(str_ends_with($this->getUrl(), 'api/' . $this->branchInfo['project'] . '/sample.php/function/sample_function/' . $this->branchInfo['branch_name']), 'Object pages redirect failed.');

    // Listing pages.
    $this->drupalGet('api/' . $this->branchInfo['project'] . '/files/' . $this->branchInfo['branch_name'] . '.x');
    $this->assertTrue(str_ends_with($this->getUrl(), 'api/' . $this->branchInfo['project'] . '/files/' . $this->branchInfo['branch_name']), 'File pages redirect failed.');
  }

}

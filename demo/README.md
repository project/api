# Demo script

If you want to test the module, copy the `api-demo.sh` script to the folder
where you would like to run the demo and then run `./api-demo.sh`. Make sure
that the script have executable permissions.

Alternatively, you can run the following command to be run in `bash`:
```
source <(curl -s https://git.drupalcode.org/project/api/-/raw/2.x/demo/api-demo.sh)
```

You need to have [ddev](https://ddev.readthedocs.io/en/stable/) installed in
the machine where you want to run this demo.

If you want Drupal core, it's better to do it through the QuickWizard UI and
only target the 11.x branch of https://git.drupalcode.org/project/drupal.git.
After doing so, you need to run the `cron` job and the `api_parse_queue` queue,
which will take a bit longer than the other projects, but you will end up
with a full Drupal 11 copy.

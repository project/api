#!/bin/bash

# IMPORTANT:
# `ddev` is required: https://ddev.readthedocs.io/en/stable/

# Clean up before we start?
# ddev delete apidemo --omit-snapshot -y
# rm -rf apidemo

# Base folders.
mkdir -p apidemo
cd apidemo
pwd

# Launch D10 site with drush and the api module brought via composer.
ddev config --project-type=drupal --docroot=web --php-version 8.3
ddev start
ddev composer create drupal/recommended-project -y
ddev composer require drush/drush
ddev composer require 'drupal/api:2.x-dev@dev' -W
ddev drush site:install --account-name=admin --account-pass=admin -y

# Enable api module and any others you might need.
ddev drush -y en api

# Bring some projects and parse them.
ddev drush api:quick-wizard https://git.drupalcode.org/project/examples.git
ddev drush cron
ddev drush queue:run api_parse_queue

ddev drush api:quick-wizard https://git.drupalcode.org/project/config_notify.git
ddev drush cron
ddev drush queue:run api_parse_queue

ddev drush api:quick-wizard https://git.drupalcode.org/project/project_browser.git
ddev drush cron
ddev drush queue:run api_parse_queue

# If you want Drupal core, it's better to do it through the QuickWizard UI and
# only target the 11.x branch of https://git.drupalcode.org/project/drupal.git
# After doing so, you need to run the cron job and the api_parse_queue queue,
# which will take a bit longer than the other projects, but you will end up
# with a full Drupal 11 copy.

ddev drush -y role:perm:add anonymous 'access API reference'
ddev drush cr

# Front-end page.
ddev launch api/projects

# Admin page.
ddev drush user:login admin/config/development/api

# Remove all the things.
# ddev delete apidemo --omit-snapshot -y
# cd .. && rm -rf apidemo
